#!/bin/bash

# Save the file in the folder /usr/sbin/so-ip-update

. $(dirname $0)/so-common

if [ -z "$OLD_IP" ]; then
    OLD_IP=$(lookup_pillar "managerip")
fi

if [ -z "$NEW_IP" ]; then
    iface=$(lookup_pillar "mainint" "host")
    NEW_IP=$(ip -4 addr list $iface | grep inet | cut -d' ' -f6 | cut -d/ -f1)
fi

if [ "$OLD_IP" != "$NEW_IP" ]; then
    for file in $(grep -rlI $OLD_IP /opt/so/saltstack /etc); do
        echo "Updating file: $file"
        sed -i "s|$OLD_IP|$NEW_IP|g" $file
    done
    sleep 1m
    reboot
else
    echo "No Changes required !!!"
fi