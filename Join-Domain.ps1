# This script will join the host to Domain controller
# powershell -ExecutionPolicy bypass "IEX((new-object net.webclient).downloadstring('https://gitlab.com/ssanthosh243/bifrost-lab-scripts/-/raw/master/Join-Domain.ps1'));Invoke-JoinDC -DomainControllerIP {pvt_ip};"


function Set-PrivateIpAddress {
    Param(
        [string]$dcip
    )
    #Setting Private IP Address
    $adapter = Get-WmiObject Win32_NetworkAdapterConfiguration | Where-Object { $_.IPAddress -And $_.IPAddress -like '169.254.*' }
    $adapterindex = $adapter.InterfaceIndex
    if ($adapter) {
        $IpAddress = (Invoke-WebRequest 'http://169.254.169.254/v1/interfaces/1/ipv4/address' -UseBasicParsing -ErrorAction SilentlyContinue).content
        if ($IpAddress) {
            New-NetIPAddress -InterfaceIndex $adapterindex -IPAddress $IpAddress -PrefixLength 20 -AddressFamily IPv4 -DefaultGateway $dcip -ErrorAction Continue
            Set-NetIPInterface -InterfaceIndex $adapterindex -NlMtuBytes 1450 -ErrorAction Continue
            Set-DNSClientServerAddress -InterfaceIndex $adapterindex  -ServerAddresses $dcip -ErrorAction Continue
        } else {
            Write-Host 'Unable to fetch Private IP.'
        }
    } else {
        Write-Host 'No Interface found with APIPA Address.'
    }
}

function Enable-NetworkAndFileSharing {
    Set-NetFirewallRule -DisplayGroup "Network Discovery" -Enabled True -Profile Any
    Set-NetFirewallRule -DisplayGroup "File And Printer Sharing" -Enabled True -Profile Any
}

function Create-SMBShare {
    if (-not (Test-Path 'C:\Share')){
        New-Item 'C:\Share' -ItemType Directory
    }
    New-SmbShare -Name 'Share' -Path 'C:\Share' | Grant-SmbShareAccess -AccountName Everyone -AccessRight Full -Force
}

function Invoke-JoinDC {
    Param(
        [Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelineByPropertyName=$True,Position=1)]
        [ValidateNotNullOrEmpty()]
        [System.String]
        $DomainControllerIP
    )
    Set-PrivateIpAddress -dcip $DomainControllerIP
    Enable-NetworkAndFileSharing

    # Creating Authentication Creds
    $user = "BIFROST.local\Administrator"
    $pass = ConvertTo-SecureString 'P@$$w0rd9' -AsPlainText -Force
    $DomainCred = New-Object System.Management.Automation.PSCredential $user, $pass

    # Adding to domain
    if (Test-Connection -ComputerName 'BIFROST.local' -Quiet) {
        Add-Computer -DomainName 'BIFROST.local' -credential $DomainCred -ComputerName $env:computername -ErrorAction Continue
    } else {
        Write-Host 'DNS Resolution to BIFROST.local failed, Please join the host manually'
    }
}
