# This script will setup active directory in Lab
# powershell -ExecutionPolicy bypass "IEX(New-Object Net.WebClient).downloadString('https://gitlab.com/ssanthosh243/bifrost-lab-scripts/-/raw/master/Create-Domain.ps1')"

$Global:Domain = 'BIFROST.local'

function Create-AdminUserAccount {
    # Creat Admin Account
    $computerName = $env:COMPUTERNAME
    $adminPassword = 'P@$$w0rd9'
    $adminUser = [ADSI] "WinNT://$computerName/Administrator,User"
    $adminUser.SetPassword($adminPassword)
}

function Set-PrivateIpAddress {
    #Setting Private IP Address
    $adapter = Get-WmiObject Win32_NetworkAdapterConfiguration | Where-Object { $_.IPAddress -And $_.IPAddress -like '169.254.*' }
    $adapterindex = $adapter.InterfaceIndex
    if ($adapter) {
        $IpAddress = (Invoke-WebRequest 'http://169.254.169.254/v1/interfaces/1/ipv4/address' -UseBasicParsing -ErrorAction SilentlyContinue).content
        if ($IpAddress) {
            New-NetIPAddress -InterfaceIndex $adapterindex -IPAddress $IpAddress -PrefixLength 20 -AddressFamily IPv4 -ErrorAction Continue
            Set-NetIPInterface -InterfaceIndex $adapterindex -NlMtuBytes 1450 -ErrorAction Continue
            Set-DNSClientServerAddress -InterfaceIndex $adapterindex  -ServerAddresses $IpAddress -ErrorAction Continue
        } else {
            Write-Host 'Unable to fetch Private IP.'
        }
    } else {
        Write-Host 'No Interface found with APIPA Address.'
    }
}

function Install-ADDS {
    # Installing Active Directory Services
    Install-WindowsFeature -name AD-Domain-Services -IncludeManagementTools -ErrorAction Continue
}

function Setup-DomainController {
    # Configure the first domain controller in a new Active Directory forest

    $PlainPassword = 'P@$$w0rd9'
    $SecurePassword = $PlainPassword | ConvertTo-SecureString -AsPlainText -Force

    Import-Module ADDSDeployment
    Install-ADDSForest `
    -SafeModeAdministratorPassword $SecurePassword `
    -CreateDnsDelegation:$false `
    -DatabasePath "C:\Windows\NTDS" `
    -DomainMode "WinThreshold" `
    -DomainName "BIFROST.local" `
    -DomainNetbiosName "BIFROST" `
    -ForestMode "WinThreshold" `
    -InstallDns:$true `
    -LogPath "C:\Windows\NTDS" `
    -NoRebootOnCompletion:$false `
    -SysvolPath "C:\Windows\SYSVOL" `
    -Force:$true
}

function Create-SMBShare {
    if (-not (Test-Path 'C:\Shares\Reports')) {
        New-Item 'C:\Shares\Reports' -ItemType Directory
    }
    New-SmbShare -Name 'Reports' -Path 'C:\Shares\Reports' | Grant-SmbShareAccess -AccountName Everyone -AccessRight Full -Force
}

function VulnAD-DisableSMBSigning {
    Set-SmbClientConfiguration -RequireSecuritySignature 0 -EnableSecuritySignature 0 -Confirm -Force
}

function Create-ADUsers {
    $UserList = "$env:USERPROFILE\Desktop\AD-Users.csv"
    if (-not (Test-Path $UserList)) {
        Invoke-WebRequest -Uri 'https://gitlab.com/ssanthosh243/bifrost-lab-scripts/-/raw/master/AD-Users.csv' -UseBasicParsing -OutFile $UserList
    }
    $Users = Import-Csv $UserList
    foreach ($user in $Users) {
        $name = $user.'name.first' + " " + $user.'name.last'
        $PrincipleName = $user.email.Split('@')[0] + '@BIFROST.local'
        New-ADUser `
            -GivenName $user.'name.first' `
            -Surname $user.'name.last' `
            -Name $name `
            -DisplayName $name `
            -SamAccountName $user.email.Split('@')[0] `
            -UserPrincipalName $PrincipleName `
            -AccountPassword (ConvertTo-SecureString $user.'login.password' -AsPlainText -Force) `
            -CannotChangePassword:$true `
            -Enabled:$true `
            -PassThru | Enable-ADAccount
    }
}

function Add-UserstoAdminGroup {
    Add-ADGroupMember -Identity 'Administrators' -Members sofia.lucas, marvin.ryan, ben.marshall
}

function Invoke-ActiveDirectorySetup {
    Start-Sleep -Seconds 60

    # Logging the output to track any errors
    $ErrorActionPreference="SilentlyContinue"
    Stop-Transcript | out-null
    Start-Transcript -path C:\tools\AD-Setup.log -append
    
    if (-not (Test-Path 'C:\Windows\SYSVOL')) {
        Set-PrivateIpAddress
        Create-AdminUserAccount
        Install-ADDS
        Setup-DomainController
    } elseif ((Test-Path 'C:\Windows\SYSVOL') -and ((Get-ADUser -Filter * | Measure-Object).Count -lt 4)) {
        Create-SMBShare
        Set-ADDefaultDomainPasswordPolicy -Identity $Global:Domain -LockoutDuration 00:01:00 -LockoutObservationWindow 00:01:00 -ComplexityEnabled $false -ReversibleEncryptionEnabled $False -MinPasswordLength 4
        Create-ADUsers
        Add-UserstoAdminGroup
    }
    Stop-Transcript
}

